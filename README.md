# UAV Simulator - GIPSA-Lab



## Simulateur générique DRONE X4 MATLAB Simulink

Simulateur développé au GIPSA-Lab à Grenoble : modélisation exhaustive des actionneurs et boucles de régulation 
Simulation validée sur les données de vol en environement intérieur.

L'ensemble de la modélisation physique est résumée dans le document PDF joint à ce dépot.


