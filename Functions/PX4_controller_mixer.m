%% ________________________  UAV X4 SIMULATOR  ____________________________
% -------- Simulateur du drone modulaire pour la recherche (DMR) ----------
% ------------- Réalisé pour l'axe UAV du programme TIRREX ----------------
% ----------------------- EQUIPEX+ ANR-21-ESRE-0015 -----------------------
%
% -------------------- LICENCE Open Source : A DEFINIR --------------------

function PWM = PX4_controller_mixer(input) 

    Gamma_c = input(1:3) ;
%     Thrust = input(4)/(m*g)*0.4 + 0.4 ;
    Thrust = input(4) ;
    Command = [Gamma_c ; Thrust] ;

    

%     Mix_PX4 = [ -493.60505957  494.0202466   494.76469807  -495.06968268 ;
%         706.35946176  -705.16731027   705.17619741  -706.41923534 ;
%         769.77107222   997.58674013  -762.22204198 -1003.15163853 ;
%        1000.50834579  1001.33544421  1001.91327278  997.0079099 ] ;
%        
    Mix_PX4 = [ -495  495 495  -495 ;
                 -705  -705   705 705  ;
                762 -762 762 -762 ;
               1000 1000 1000 1000 ]' ;
          
%     offset_Mix_PX4 = [ 999.34394837 ;  999.07837025 ;  998.90433137 ; 1000.43599055] ;
	offset_Mix_PX4 = [1000 1000 1000 1000]' ;

    PWM = (Mix_PX4*Command + offset_Mix_PX4)' ;


end

