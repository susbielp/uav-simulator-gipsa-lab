%% ________________________  UAV X4 SIMULATOR  ____________________________
% -------- Simulateur du drone modulaire pour la recherche (DMR) ----------
% ------------- Réalisé pour l'axe UAV du programme TIRREX ----------------
% ----------------------- EQUIPEX+ ANR-21-ESRE-0015 -----------------------
%
% -------------------- LICENCE Open Source : A DEFINIR --------------------

function state_dot_and_Thrust = Full_state_dot_quaternion(input, J, inv_J, S_apparente, rotor_position_mat, Jr, cT, cQ, m, g, Cx, rho_air, rho_ground,rotor_radius,hauteur_rotor)


    vx = input(1)    ;
    vy = input(2)    ;
    vz = input(3)    ;
    phi = input(4)   ;
    theta = input(5) ;
    psi = input(6)   ;
    
    p = input(7)     ;
    q = input(8)     ;
    r = input(9)     ;
    Omega = [p q r]' ;
    
    q0 = input(10) ;
    q1 = input(11) ;
    q2 = input(12) ;
    q3 = input(13) ;
    quaternion = [q0 q1 q2 q3]' ;
    
    W1 = input(14)   ;
    W2 = input(15)   ;
    W3 = input(16)   ;
    W4 = input(17)   ;
    
    Gamma_p_localDist = input(18) ;
    Gamma_q_localDist = input(19) ;
    Gamma_r_localDist = input(20) ;
    
    Force_globalDist_x = input(21) ;
    Force_globalDist_y = input(22) ;
    Force_globalDist_z = input(23) ;
    Force_globalDist = [Force_globalDist_x Force_globalDist_y Force_globalDist_z]' ;
    
     
    Force_localDist_x = input(24) ;
    Force_localDist_y = input(25) ;
    Force_localDist_z = input(26) ;
    Force_localDist = [Force_localDist_x Force_localDist_y Force_localDist_z]' ;
    
    
    vx_wind = input(27) ;
    vy_wind = input(28) ;
    vz_wind = input(29) ;
    
    % quaternion = euler2quaternion([phi theta psi]) ;
    % q0 = quaternion(1) ;
    % q1 = quaternion(2) ;
    % q2 = quaternion(3) ;
    % q3 = quaternion(4) ;
   
    % Position callback 
    sf_x = input(30) ;
    sf_y = input(31) ;
    sf_z = input(32) ;


    %%%%%%%%%%%%%%%%%%%%%%%%%% INNER MIXER %%%%%%%%%%%%%%%%%%%%%%%
    
    Wi = [W1 W2 W3 W4]' ; 
    
    
    % R_matrix = rotation_matrix_from_Euler_123([phi theta psi]) ;
    
    
    % FLAPPING NEW VERSION
    R1_flap = eye(3) ;
    R2_flap = eye(3) ;
    R3_flap = eye(3) ;
    R4_flap = eye(3) ;
    
     
    
    % Repère local 
    b1 = [1  0  0]' ;
    b2 = [0  1  0]' ;
    b3 = [0  0  1]' ;
    
    e1 = [1  0  0]' ;
    e2 = [0  1  0]' ;
    e3 = [0  0  1]' ;
    
    F1 = R1_flap*cT*W1^2*b3 ;
    F2 = R2_flap*cT*W2^2*b3 ;
    F3 = R3_flap*cT*W3^2*b3 ;
    F4 = R4_flap*cT*W4^2*b3 ;
    
    
    % Command_2 = inv(Mx)*[W1^2 W2^2 W3^2 W4^2]' ;
    % Gamma_yaw = Command_2(4) ;  
    Gamma_yaw = (cQ*W1^2 - cQ*W2^2 + cQ*W3^2 - cQ*W4^2)*b3 ;
    Gamma_c_int = cross(rotor_position_mat(1,:)', F1) + cross(rotor_position_mat(2,:)', F2) + cross(rotor_position_mat(3,:)', F3) + cross(rotor_position_mat(4,:)', F4) ;



    Gamma_gyro = Jr*cross(Omega, b3)*(-1)*(W1 - W2 + W3 - W4) ; 
    Gamma_c = Gamma_c_int + Gamma_yaw + [Gamma_p_localDist Gamma_q_localDist Gamma_r_localDist]' + Gamma_gyro;
    Thrust = (F1 + F2 + F3 + F4);
    
    
    % --------------------  GROUND EFFECT TURHST --------------------------
    % (mieux sur chaque moteur mais ok déjà)
%     rho_ground = 1.4 ;
%     rotor_radius = 0.06 ;
%     hauteur_rotor = 0.0293 ;

    if sf_z >= 0 
        zr = sf_z + hauteur_rotor ;
        coef_ground = 1 - rho_ground*(rotor_radius/(4*zr))^2 ;
        grounded_thrust = Thrust/coef_ground ;
    else
        grounded_thrust = Thrust ;
    end

    Thrust = grounded_thrust ;




    %%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%% BOUCLE PQR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Gamma_c = [Gamma_p Gamma_q Gamma_r]' ; 
    
    Omega = [p q r ]' ;
    Omega_skew = [0 -r q ;
                  r 0 -p ;
                 -q p 0] ;
    
    Omega_dot = -inv_J*Omega_skew*J*Omega + inv_J*Gamma_c ;
    p_dot = Omega_dot(1) ;
    q_dot = Omega_dot(2) ;
    r_dot = Omega_dot(3) ;
    
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%% BOUCLE ATTITUDE %%%%%%%%%%%%%%%%%%%%%
    
    
    
    phi_dot = p + sin(phi)*tan(theta)*q + cos(phi)*tan(theta)*r ;
    theta_dot = cos(phi)*q - sin(phi)*r ;
    psi_dot = (sin(phi)/cos(theta))*q + (cos(phi)/cos(theta))*r ;
    
    Euler_123_dot = [phi_dot theta_dot psi_dot]' ;
    
     
    quaternion_vector_skew = [0 -q3 q2 ;
                             q3 0 -q1 ;
                             -q2 q1 0] ;
    quaternion_dot = [ -(0.5)*[q1 q2 q3]*Omega ;
                      (0.5)*(quaternion_vector_skew + q0*eye(3))*Omega] ;
    % quaternion_dot = (0.5)*quaternion_product(quaternion, [0; Omega]) ;
    q0_dot = quaternion_dot(1) ;
    q1_dot = quaternion_dot(2) ;
    q2_dot = quaternion_dot(3) ;
    q3_dot = quaternion_dot(4) ;
    
    % q0_dot = quaternion_dot(2) ;
    % q1_dot = -quaternion_dot(1) ;
    % q2_dot = quaternion_dot(4) ;
    % q3_dot = quaternion_dot(3) ;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%% BOUCLE POS SPEED %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    R_matrix_quat = [(q0^2 + q1^2 - q2^2 - q3^2)  2*(q1*q2 - q0*q3) 2*(q1*q3 + q0*q2) ;
                     2*(q1*q2 + q0*q3)  (q0^2 - q1^2 + q2^2 - q3^2) 2*(q2*q3 - q0*q1) ;
                     2*(q1*q3 - q0*q2) 2*(q2*q3 + q0*q1) (q0^2 - q1^2 - q2^2 + q3^2) ] ;
    
    % R_matrix = rotation_matrix_from_Euler_123([phi theta psi]) ;


    R_matrix = R_matrix_quat ;
    Projected_apparent_wind = R_matrix'*[-vx+vx_wind -vy+vy_wind -vz+vz_wind]' ;


    F_wind_x_local = (0.5)*rho_air*Cx*S_apparente(1,1)*abs(Projected_apparent_wind(1))*(Projected_apparent_wind(1))*e1  ;
    F_wind_y_local = (0.5)*rho_air*Cx*S_apparente(2,2)*abs(Projected_apparent_wind(2))*(Projected_apparent_wind(2))*e2  ;
    F_wind_z_local = (0.5)*rho_air*Cx*S_apparente(3,3)*abs(Projected_apparent_wind(3))*(Projected_apparent_wind(3))*e3  ;
    
    F_wind_local = F_wind_x_local + F_wind_y_local + F_wind_z_local ;
   
   
    accelerations = (1/m)*(-g*m*e3 + R_matrix*Thrust + R_matrix*F_wind_local + Force_globalDist + R_matrix*Force_localDist) ; 
    
    
    % quaternion_conjuge = [q0 -q1  -q2 -q3]' ;
    % quat_prod = quaternion_product(quaternion, quaternion_product([0 ; Thrust],quaternion_conjuge))
    % accelerations = quat_prod(2:end) - g*[0 0 1]';
    
    
    
    
    x_dot_dot = accelerations(1) ;
    y_dot_dot = accelerations(2) ;
    z_dot_dot = accelerations(3) ;
    
    
    speed_dot = [x_dot_dot  y_dot_dot  z_dot_dot] ;
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    state_dot = [x_dot_dot y_dot_dot z_dot_dot phi_dot theta_dot psi_dot p_dot q_dot r_dot q0_dot q1_dot q2_dot q3_dot] ;
    % state_dot = [x_dot_dot  y_dot_dot  z_dot_dot q0_dot q1_dot q2_dot q3_dot p_dot q_dot r_dot] ;
    
    state_dot_and_Thrust = [state_dot, Thrust'] ;

end

