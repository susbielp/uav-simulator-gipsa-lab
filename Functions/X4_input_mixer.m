%% ________________________  UAV X4 SIMULATOR  ____________________________
% -------- Simulateur du drone modulaire pour la recherche (DMR) ----------
% ------------- Réalisé pour l'axe UAV du programme TIRREX ----------------
% ----------------------- EQUIPEX+ ANR-21-ESRE-0015 -----------------------
%
% -------------------- LICENCE Open Source : A DEFINIR --------------------

function Ui = X4_input_mixer(input)
    
    PWM1 = input(1) ;
    PWM2 = input(2) ;
    PWM3 = input(3) ;
    PWM4 = input(4) ;

    U_bat = input(5) ;

    PWM_refs = [PWM1; PWM2; PWM3; PWM4];
    U = PWM_data_map_vectorized(1000, 2000, 0, U_bat, PWM_refs);
    
    U1 = U(1);
    U2 = U(2);
    U3 = U(3);
    U4 = U(4);


%     U1 = PWM_data_map(1000, 2000, 0, U_bat, PWM1) ;
%     U2 = PWM_data_map(1000, 2000, 0, U_bat, PWM2) ;
%     U3 = PWM_data_map(1000, 2000, 0, U_bat, PWM3) ;
%     U4 = PWM_data_map(1000, 2000, 0, U_bat, PWM4) ;

    Ui = [U1 U2 U3 U4] ;
    
end


