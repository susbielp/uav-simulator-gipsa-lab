%% ________________________  UAV X4 SIMULATOR  ____________________________
% -------- Simulateur du drone modulaire pour la recherche (DMR) ----------
% ------------- Réalisé pour l'axe UAV du programme TIRREX ----------------
% ----------------------- EQUIPEX+ ANR-21-ESRE-0015 -----------------------
%
% -------------------- LICENCE Open Source : A DEFINIR --------------------

function outputs = PWM_data_map_vectorized(low1, high1, low2, high2, data_vec)

    % Calculate slope
    pente = (high2 - low2) / (high1 - low1);
    
    % Calculate offset using average
    offset = 0.5 * (low2 + high2 - pente * (low1 + high1));
    
    % Map and clamp values
    outputs = pente * data_vec + offset;
    outputs = max(min(outputs, high2), low2);

end
