%% ________________________  UAV X4 SIMULATOR  ____________________________
% -------- Simulateur du drone modulaire pour la recherche (DMR) ----------
% ------------- Réalisé pour l'axe UAV du programme TIRREX ----------------
% ----------------------- EQUIPEX+ ANR-21-ESRE-0015 -----------------------
%
% -------------------- LICENCE Open Source : A DEFINIR --------------------

function Wi_des = X4_input_mixer_CLOSED_LOOP_ESCs(input)


%     global Mx Kv_motor Kv_motor_rad U_sat 
%     global Closed_Loop_ESC_wMin Closed_Loop_ESC_wMax

    Closed_Loop_ESC_wMin = input(5) ;
    Closed_Loop_ESC_wMax = input(6) ;

%     Wi_max = Kv_motor_rad*U_sat ;
    RPM_ref_1 = input(1) ;
    RPM_ref_2 = input(2) ;
    RPM_ref_3 = input(3) ;
    RPM_ref_4 = input(4) ;


%     W1_des = PWM_data_map(1000, 2000, Closed_Loop_ESC_wMin*2*pi/60, Closed_Loop_ESC_wMax*2*pi/60, RPM_ref_1) ;
%     W2_des = PWM_data_map(1000, 2000, Closed_Loop_ESC_wMin*2*pi/60, Closed_Loop_ESC_wMax*2*pi/60, RPM_ref_2) ;
%     W3_des = PWM_data_map(1000, 2000, Closed_Loop_ESC_wMin*2*pi/60, Closed_Loop_ESC_wMax*2*pi/60, RPM_ref_3) ;
%     W4_des = PWM_data_map(1000, 2000, Closed_Loop_ESC_wMin*2*pi/60, Closed_Loop_ESC_wMax*2*pi/60, RPM_ref_4) ;

    RPM_refs = [RPM_ref_1; RPM_ref_2; RPM_ref_3; RPM_ref_4];
    W_des = PWM_data_map_vectorized(1000, 2000, Closed_Loop_ESC_wMin*2*pi/60, Closed_Loop_ESC_wMax*2*pi/60, RPM_refs);
    
    W1_des = W_des(1);
    W2_des = W_des(2);
    W3_des = W_des(3);
    W4_des = W_des(4);

%     W1_des = PWM_data_map(1000, 2000, Closed_Loop_ESC_wMin, Closed_Loop_ESC_wMax, RPM_ref_1) ;
%     W2_des = PWM_data_map(1000, 2000, Closed_Loop_ESC_wMin, Closed_Loop_ESC_wMax, RPM_ref_2) ;
%     W3_des = PWM_data_map(1000, 2000, Closed_Loop_ESC_wMin, Closed_Loop_ESC_wMax, RPM_ref_3) ;
%     W4_des = PWM_data_map(1000, 2000, Closed_Loop_ESC_wMin, Closed_Loop_ESC_wMax, RPM_ref_4) ;

%     W1_des = PWM_data_map(1000, 2000, 0, Wi_max, input(1)) ;
%     W2_des = PWM_data_map(1000, 2000, 0, Wi_max, input(2)) ;
%     W3_des = PWM_data_map(1000, 2000, 0, Wi_max, input(3)) ;
%     W4_des = PWM_data_map(1000, 2000, 0, Wi_max, input(4)) ;

    Wi_des = [W1_des W2_des W3_des W4_des] ;
end

