%% ________________________  UAV X4 SIMULATOR  ____________________________
% -------- Simulateur du drone modulaire pour la recherche (DMR) ----------
% ------------- Réalisé pour l'axe UAV du programme TIRREX ----------------
% ----------------------- EQUIPEX+ ANR-21-ESRE-0015 -----------------------
%
% -------------------- LICENCE Open Source : A DEFINIR --------------------
% --------- Maintener: pierre.susbielle@gipsa-lab.grenoble-inp.fr ---------


% Documentation et source : [lien git]


clear all
close all
clc

Ts = 0.01 ;             % Période d'échantillonage contrôle débarqué [s]
g = 9.81 ;              % Accélération de pesanteur [ms.-2]


%% ______________ I. DRONE HARDWARE AND AUTOPILOT MODEL ___________________

%%% ____________________ I.1 MODELE DYNAMIQUE  ____________________________


% ------------------------ GEOMETRIE CHASSIS ------------------------------


% _____                               _____
%| 2(-)| --lp_left--  |  --lp_right--| 1(+)|
%|_____|              |              |_____|
%      \              |             /
%       \             |            /
%        \        lp_front        /
%         \           |          /
%          \          |         /
%           \         |        /
%           /         |        \
%          /          |         \
%         /           |          \     
%        /        lp_back         \  
%       /             |            \  
%      /              |             \
%     /               |              \
% _____               |               _____
%| 3(+)|              |              | 4(-)|
%|_____|                             |_____|
      


% Positions des rotors dans le repère local :
lp_left = (222/2)*1e-3 ;        % Lever arm -projected distances- [m]
lp_right = (222/2)*1e-3 ;       % Lever arm -projected distances- [m]
lp_front = (170/2)*1e-3 ;       % Lever arm -projected distances- [m]
lp_back = (170/2)*1e-3 ;        % Lever arm -projected distances- [m]
h_chassis = 0.15 ;              % Total frame height [m]
h_rotor = 0.030 ;               % Motor rotation plane height [m]

         
% Rotors position matrix (x, y, z) in {B}


rotor_position_mat = [lp_front    -lp_right     h_rotor ;
                       lp_front   lp_left       h_rotor ;
                      -lp_back    lp_left       h_rotor ;
                      -lp_back    -lp_right     h_rotor] ;

m = 1.617 ;                         % Masse [kg]
hover_compensation = 0.39 ;         % Hover thrust for normalization [-]
                                    % To be measured inflight to normalized
                                    % the thrust from Newtons to [0-1][-]

 
% ------------------------- INERTIE TOTALE --------------------------------
%                - ramenée en G, repère local {B} -

Ix = 5686.57/(1000^2) ;      % Moment of inertia x [-]
Ixy = -0.045/(1000^2) ;
Ixz = -17.253/(1000^2) ;
Iyx = -0.045/(1000^2) ;
Iy = 5413.986/(1000^2) ;     % Moment of inertia y [-]
Iyz = -0.774/(1000^2) ;   
Izx = -17.253/(1000^2) ;
Izy = -0.774/(1000^2) ;
Iz = 8946.234/(1000^2) ;     % Moment of inertia z [-]

J = [Ix Ixy Ixz ;
     Iyx Iy Iyz ;
     Izx Izy Iz] ;                      % Matrice d'inertie [-]
 
inv_J = inv(J) ;


% ---------- Modélisation des Frottements aérodynamiques -----------------
% Dans le repère local : F = (1/2)*Cx*rho_air*S*[vx vy vz].^2 

rho_air = 1.293 ;           % Masse volumique de l'air [kg.m^-3] 
Cx = 1.05 ;                 % Coefficient frottement aero rectangle [-]

S_apparente_front = h_chassis*(lp_left + lp_right) ; 
                            % Surface apparente [m.-2]   
S_apparente_side = h_chassis*(lp_front + lp_back) ;   
                            % Surface apparente [m.-2]   
S_apparente_top = (lp_front + lp_back)*(lp_left + lp_right) ;  
                            % Surface apparente [m.-2]   
S_apparente =  diag([S_apparente_front S_apparente_side S_apparente_top]) ;
                            % Surface apparente [m.-2]   




%%% ________________ I.2 ACTIONNEURS ET PUISSANCE _________________________

% ------------------------- PARAMETRES MOTEUR -----------------------------
% Type de moteur : T-Motor F60 v IV 1750kv
% D'apres l'OUTIL ci-joint et les données constructeur

Kv_motor = 1750 ;                       % Constante moteur [tr.min-1] 
Kv_motor_rad = Kv_motor*2*pi/60 ;       % Constante moteur [rad.s-1]
km = 1/Kv_motor_rad ;

rotor_radius = 6.0*1e-2 ;               % Diamètre moteur [m]
hauteur_rotor = 29.3*1e-3 ;             % Hauteur moteur [m]

Jr = 1.2991*1e-05 ;                     % Moment d'inertie moteur [a definir]

R = 0.07 ;                              % Résistance interne [Ohm]
L_mot = 1.0000e-04 ;                    % Inductance interne [H]


% ----------------------- PARAMETRES HELICES -----------------------------

with_flapping = 0 ;                     % Seclect flapping in simulation [0/1] [FOR NOW 0]
with_ground_effect = 1 ;                % Seclect ground effect in simulation [0/1]
    
% Type d'hélice : 6145 AZURE POWER.
% Depuis Outil ci-oint et data constructeur: 
cT = 1.3529e-06 ;                       % Coefficient de poussée [-]
cQ = 2.1113*1e-08 ;                     % Coefficient de trainée [-]

rho_ground = 1.4 ;                      % Effet de sol [-]

                  

% ------------------------- PARAMETRES ESC --------------------------------
% Valeurs réglées pour les ESCs boucle fermée sous AM32 et mixeur par
% défault PX4 : 

CLOSED_LOOP_ESCs = 1 ;                  % Select ESC Configuration [0/1] 
                                        % Useless 
Closed_Loop_ESC_wMin = 3000 ;           % Vitesse moteur minimale [RPM]
Closed_Loop_ESC_wMax = 24000 ;          % Vitesse moteur maximale [RPM]



% ------------------- PARAMETRES BATTERIE [1-6S] --------------------------
nb_battery_cells = 4 ;                  % Nombre de cellule de batterie [-]  
U_sat = 3.7*nb_battery_cells ;          % Tension nominale batterie [V]
R_bat = 0.024 ;                         % Résistance interne batterie [Ohm]




%%% _______________________ I.3 AUTOPILOTE ________________________________
% --------------------- Autopilote embarqué PX4 ---------------------------


% -------------------------- Matrice de mix -------------------------------
Mx = [ cT        cT      cT         cT ; 
      -cT*lp_right   cT*lp_left    cT*lp_left   -cT*lp_right ;
      -cT*lp_front   -cT*lp_front    cT*lp_back   cT*lp_back ;
      cQ         -cQ      cQ     -cQ ] ; 

Mx_inv = inv(Mx) ;

% --------------------- PX4 Angular Rates Control Loop --------------------
P_ROLL = 0.08 ;
I_ROLL = 0.250 ; 
D_ROLL = 0.001 ; 

P_PITCH = 0.08 ;
I_PITCH = 0.2 ;
D_PITCH = 0.002 ; 

P_YAW = 0.2 ;
I_YAW = 0.1 ; 
D_YAW = 0.0 ;

K_PQR_PX4 = 1*diag([1 1 1]) ;
P_PQR_PX4 = diag([P_ROLL P_PITCH P_YAW]);
I_PQR_PX4 = diag([I_ROLL I_PITCH I_YAW]) ;
D_PQR_PX4 = diag([D_ROLL D_PITCH D_YAW]) ;


% -------------------- PX4 Attitude Control Loop --------------------------
P_ATTITUDE_PX4 = diag([8 8 4]) ;          % Quaternion attitude loop gain [-]

max_angle_deg = 45 ;                      % Angle de saturation autopilote [deg]
max_angle = max_angle_deg*pi/180 ;        % Angle de saturation autopilote [rad]


% ----------------------- PX4 Loop Frequencies ----------------------------

PX4_rate_loop_frequency = 500 ;           % Fréquence boucle rates [Hz]
PX4_attitude_loop_frequency = 250 ;       % Fréquence boucle attitude [Hz]



%%% ________________________ I.4 CAPTEURS _________________________________
% Système de capture de mouvement VICON TRACKER
% IMU embarquée [modèle simple]
% Pas de modèlisation de filtre de Kalman pour l'instant 


% Modelisation Vicon Tracer 
ViconTracker_noise_variance = 0.000001 ;    % Vicon noise variation [m^2]
ViconTracker_noise_mean = 0.00001 ;         % Vicon noise mean [m]


% Modelisation IMU [PAS UTILISE ENCORE, A VERIFIER ET COMPLETER]
IMU_noise_variance = 0.000001 ;    % IMU noise variation [rad.s]
IMU_noise_mean = 0.00001 ;         % IMU noise mean [rad.s-2]
IMU_bais = 0.000001 ;              % IMU bais [rad]





%% ___________________ II. Simulation Scenario setup ______________________

%%% ------------------ Vent 3D repère global {E} --------------------------
vx_wind = 0 ;                   % Composante x vent [m.s-1]
vy_wind = 0 ;                   % Composante y vent [m.s-1]
vz_wind = 0 ;                   % Composante z vent [m.s-1]
vitesse_vent = [vx_wind vy_wind vz_wind]' ;


%%% --------- Conditions initiales position - vitesse - attitude ----------

initial_speed_conditions = 0*[1 1 1] ;          % Position initiale [m]
initial_pos_conditions = 0*[1 1 1] ;            % Vitesse initiale [m.s-1]
initial_attitude_condition = 0*[0.5 0 0 ] ;     % Attitude initiale [rad]

if initial_pos_conditions ~= [0 0 0]
    disp("WARNING : Initial position conditions set to :")
    disp(initial_pos_conditions) 
end
if initial_speed_conditions ~= [0 0 0]
    disp("WARNING : Initial speed conditions set to :")
    disp(initial_speed_conditions) 
end


% ----------------- Générateur de trajectoire complexes -------------------
% Example 1 : Circles and upward motion

Te = Ts ;
final_time = 130 ;                           %    Final simulation time [s]
t1 = linspace(0, final_time, floor(final_time*(1/Te))) ;
Rayon = 1 ;
omega1_traj_simu = 2*pi*(1/20) ;
x_ref_from_ws = -0.1 + Rayon*cos(omega1_traj_simu*t1)  ;
y_ref_from_ws = Rayon*sin(omega1_traj_simu*t1)  ;
z_ref_from_ws = 0.2*t1 ;
x_dot_ref_from_ws = [0 diff(x_ref_from_ws)] ;
y_dot_ref_from_ws = [0 diff(y_ref_from_ws)] ;
z_dot_ref_from_ws = [0 diff(z_ref_from_ws)]  ;
ARM_from_ws = timeseries(ones(size(x_ref_from_ws)), t1)  ;

position_speed_ref_from_ws = timeseries() ;
position_speed_ref_from_ws.Time = t1' ;
position_speed_ref_from_ws.Data = [x_ref_from_ws ; y_ref_from_ws  ; z_ref_from_ws ; x_dot_ref_from_ws  ; y_dot_ref_from_ws  ; z_dot_ref_from_ws ]';

psi_ref_from_ws = timeseries(0.2*ones(size(x_ref_from_ws)), t1) ;


figure() ;
plot3(x_ref_from_ws, y_ref_from_ws, z_ref_from_ws) ; grid on ; title("Trajectoire de référence") ;
xlabel("x (m)") ; ylabel("y (m)") ; zlabel("z (m)") ;




%% III. POSITION & SPEED CONTROL 

%%% III.1 Example : Non linear PID, performances based on double integrator
%%% model.

A_DI = [0 0 0 1 0 0 ;
        0 0 0 0 1 0 ;
        0 0 0 0 0 1 ;
        0 0 0 0 0 0 ;
        0 0 0 0 0 0 ;
        0 0 0 0 0 0 ] ;

B_DI = [0 0 0
        0 0 0
        0 0 0
        1 0 0
        0 1 0
        0 0 1 ] ;

C_DI = [eye(3) zeros(3)] ;

A_DI_extended = [A_DI zeros(6,3) ;
              -C_DI zeros(3)] ;
B_DI_extended = [B_DI ; zeros(3) ] ;
K_DI = lqr(A_DI, B_DI, eye(6), eye(3)) ;
Q_DI = diag([[1 1 1], 0.5*[1 1 1], 0.5*[1 1 1]]) ;
R_DI = 0.2*eye(3) ;

K_DI_int = lqrd(A_DI_extended,   B_DI_extended, Q_DI, R_DI, Ts) 





