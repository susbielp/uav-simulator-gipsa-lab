%% ________________________  UAV X4 SIMULATOR  ____________________________
% -------- Simulateur du drone modulaire pour la recherche (DMR) ----------
% ------------- Réalisé pour l'axe UAV du programme TIRREX ----------------
% ----------------------- EQUIPEX+ ANR-21-ESRE-0015 -----------------------
%
% -------------------- LICENCE Open Source : A DEFINIR --------------------
% --------- Maintener: pierre.susbielle@gipsa-lab.grenoble-inp.fr ---------


% _______________________ UAV SIMULATOR DATA VIEWER  ______________________
% _________________ To be executed after a SIMULINK simulation ____________

close all


%%% ___________ I. DATA CALLBACK FROM TOWORKSPACE "UAV_DATA" _______________

time = UAV_DATA.Time ;
Pos_speed_ref = UAV_DATA.Data(:,1:6) ;
Euler123_ref = UAV_DATA.Data(:,7:9) ;
Thrust_ref = UAV_DATA.Data(:,10) ;
pqr_ref = UAV_DATA.Data(:,11:13) ;
UAV_ARM = UAV_DATA.Data(:,14) ;
sf_pos = UAV_DATA.Data(:,15:17) ;
sf_speed = UAV_DATA.Data(:,18:20) ;
sf_Euler123 = UAV_DATA.Data(:,21:23) ;
sf_quaterion = UAV_DATA.Data(:,24:27) ;
sf_pqr = UAV_DATA.Data(:,28:30) ;

uav_thrust = UAV_DATA.Data(:,31:33) ;
uav_MotorSpeeds = UAV_DATA.Data(:,34:37) ;
uav_MotorVoltages = UAV_DATA.Data(:,38:41) ;
uav_MotorCurrents = UAV_DATA.Data(:,42:45) ;

uav_Battery = UAV_DATA.Data(:,46) ;
uav_PWM = UAV_DATA.Data(:,47:50) ;


x_ref = Pos_speed_ref(:,1) ;
y_ref = Pos_speed_ref(:,2) ;
z_ref = Pos_speed_ref(:,3) ;
x_dot_ref = Pos_speed_ref(:,4) ;
y_dot_ref = Pos_speed_ref(:,5) ;
z_dot_ref = Pos_speed_ref(:,6) ;

phi_ref = Euler123_ref(:,1) ;
theta_ref = Euler123_ref(:,2) ;
psi_ref = Euler123_ref(:,3) ;

p_ref = pqr_ref(:,1) ;
q_ref = pqr_ref(:,2) ;
r_ref = pqr_ref(:,3) ;

sf_x = sf_pos(:,1) ;
sf_y = sf_pos(:,2) ;
sf_z = sf_pos(:,3) ;
sf_x_dot = sf_speed(:,1) ;
sf_y_dot = sf_speed(:,2) ;
sf_z_dot = sf_speed(:,3) ;
sf_phi = sf_Euler123(:,1) ;
sf_theta = sf_Euler123(:,2) ;
sf_psi = sf_Euler123(:,3) ;
sf_p = sf_pqr(:,1) ;
sf_q = sf_pqr(:,2) ;
sf_r = sf_pqr(:,3) ;
sf_qx = sf_quaterion(:,1) ;
sf_qy = sf_quaterion(:,2) ;
sf_qz = sf_quaterion(:,3) ;
sf_qw = sf_quaterion(:,4) ;



%%% ________________________ II. DATA PLOT ________________________________

% -------------------------- II.1 POSITION  -------------------------------

marginPercent = 0.05 ;  % Top and bottom plotting margin in % [-]

figure() ;
subplot(311) ; title("Position response in E") ;
plot(time, x_ref) ; hold on ; plot(time, sf_x) ; grid on ; legend("x_{ref}", "x") , xlabel("Time [s]") ; ylabel("Position [m]") ;
currentYLim = ylim;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;

subplot(312) ;
plot(time, y_ref) ; hold on ; plot(time, sf_y) ; grid on ; legend("y_{ref}", "y") , xlabel("Time [s]") ; ylabel("Position [m]") ;
currentYLim = ylim;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;

subplot(313) ; 
plot(time, z_ref) ; hold on ; plot(time, sf_z) ; grid on ; legend("z_{ref}", "z") , xlabel("Time [s]") ; ylabel("Position [m]") ;
currentYLim = ylim;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;

figure() ;
title("3D Position response in E") ;
plot3(sf_x, sf_y, sf_z) ; hold on ; grid on ; plot3(x_ref, y_ref, z_ref) ; 
legend("Measured trajectory", "Desired trajectory") , xlabel("Position x [m]") ; ylabel("Position y [m]") ; zlabel("Position z [m]") ;


% -------------------------- II.3 ATTITUDE  -------------------------------
marginPercent = 0.05 ;  % Top and bottom plotting margin in % [-]

figure() ;
subplot(311) ; title("Angular response in E (Euler X-Y-Z)") ;
plot(time, phi_ref) ; hold on ; plot(time, sf_phi) ; grid on ; 
currentYLim = ylim;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;
plot([time(1) time(end)], [max_angle max_angle], '--', 'LineWidth',1.5, 'Color','green')
plot([time(1) time(end)], -[max_angle max_angle], '--', 'LineWidth',1.5, 'Color','green')
legend("\phi_{ref}", "\phi", "Max Angle", "Min Angle") , xlabel("Time [s]") ; ylabel("Roll [rad]") ;

subplot(312) ;
plot(time, theta_ref) ; hold on ; plot(time, sf_theta) ; grid on ;  xlabel("Time [s]") ; ylabel("Attitude [rad]") ;
currentYLim = ylim;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;
plot([time(1) time(end)], [max_angle max_angle], '--', 'LineWidth',1.5, 'Color','green')
plot([time(1) time(end)], -[max_angle max_angle], '--', 'LineWidth',1.5, 'Color','green')
legend("\theta{ref}", "\theta", "Max Angle", "Min Angle") , xlabel("Time [s]") ; ylabel("Pith [rad]") ;

subplot(313) ; 
plot(time, psi_ref) ; hold on ; plot(time, sf_psi) ; grid on ; legend("\psi_{ref}", "\psi") , xlabel("Time [s]") ; ylabel("Yaw [rad]") ;
currentYLim = ylim;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;


% ------------------------ II.4 ANGULAR RATES  ----------------------------
marginPercent = 0.05 ;  % Top and bottom plotting margin in % [-]

figure() ;
subplot(311) ; title("Angular speed response in B (PQR)") ;
plot(time, p_ref) ; hold on ; plot(time, sf_p) ; grid on ; legend("p_{ref}", "p") , xlabel("Time [s]") ; ylabel("Angular speed [rad.s-1]") ;
currentYLim = ylim;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;

subplot(312) ;
plot(time, q_ref) ; hold on ; plot(time, sf_q) ; grid on ; legend("q_{ref}", "q") , xlabel("Time [s]") ; ylabel("Angular speed [rad.s-1]") ;
currentYLim = ylim;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;

subplot(313) ; 
plot(time, r_ref) ; hold on ; plot(time, sf_r) ; grid on ; legend("r_{ref}", "r") , xlabel("Time [s]") ; ylabel("Angular speed [rad.s-1]") ;
currentYLim = ylim;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;


% ----------------------- II.5 THRUST and WEIGHT  -------------------------
marginPercent = 0.05 ;  % Top and bottom plotting margin in % [-]

figure() ;
title("Thrust [N]") ;
plot(time, uav_thrust) ; hold on ; plot(time, Thrust_ref*(m*g)/hover_compensation) ; plot([time(1) time(end)], [m*g m*g], '--', 'LineWidth', 1.5, 'Color',"red") ; 
grid on ; legend("Thrust_{ref}", "Thrust", "Weight") , xlabel("Time [s]") ; ylabel("Force [N]") ;
currentYLim = ylim ;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;



% ------------------------- II.6 Motor Speeds  ----------------------------
marginPercent = 0.05 ;  % Top and bottom plotting margin in % [-]

figure() ;
subplot 411
plot(time, uav_MotorSpeeds(:,1)) ; hold on ; grid on ; 
plot([time(1), time(end)],2*pi/60*[Closed_Loop_ESC_wMin Closed_Loop_ESC_wMin],'--', 'LineWidth', 1.5, 'Color',"red") ;
plot([time(1), time(end)],2*pi/60*[Closed_Loop_ESC_wMax Closed_Loop_ESC_wMax],'--', 'LineWidth', 1.5, 'Color',"red") ;
legend("Motor1 Speed [rad.s-1]", "Max speed", "Min speed") ;
xlabel("Time [s]") ; ylabel("Rotation speed [rad.s-1]") ;
currentYLim = ylim ;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;

subplot 412
plot(time, uav_MotorSpeeds(:,2)) ; hold on ; grid on ; 
plot([time(1), time(end)],2*pi/60*[Closed_Loop_ESC_wMin Closed_Loop_ESC_wMin],'--', 'LineWidth', 1.5, 'Color',"red") ;
plot([time(1), time(end)],2*pi/60*[Closed_Loop_ESC_wMax Closed_Loop_ESC_wMax],'--', 'LineWidth', 1.5, 'Color',"red") ;
legend("Motor2 Speed [rad.s-1]", "Max speed", "Min speed") ;
xlabel("Time [s]") ; ylabel("Rotation speed [rad.s-1]") ;
currentYLim = ylim ;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;

subplot 413
plot(time, uav_MotorSpeeds(:,3)) ; hold on ; grid on ; 
plot([time(1), time(end)],2*pi/60*[Closed_Loop_ESC_wMin Closed_Loop_ESC_wMin],'--', 'LineWidth', 1.5, 'Color',"red") ;
plot([time(1), time(end)],2*pi/60*[Closed_Loop_ESC_wMax Closed_Loop_ESC_wMax],'--', 'LineWidth', 1.5, 'Color',"red") ;
legend("Motor3 Speed [rad.s-1]", "Max speed", "Min speed") ;
xlabel("Time [s]") ; ylabel("Rotation speed [rad.s-1]") ;
currentYLim = ylim ;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;

subplot 414
plot(time, uav_MotorSpeeds(:,4)) ; hold on ; grid on ; 
plot([time(1), time(end)],2*pi/60*[Closed_Loop_ESC_wMin Closed_Loop_ESC_wMin],'--', 'LineWidth', 1.5, 'Color',"red") ;
plot([time(1), time(end)],2*pi/60*[Closed_Loop_ESC_wMax Closed_Loop_ESC_wMax],'--', 'LineWidth', 1.5, 'Color',"red") ;
legend("Motor4 Speed [rad.s-1]", "Max speed", "Min speed") ;
xlabel("Time [s]") ; ylabel("Rotation speed [rad.s-1]") ;
currentYLim = ylim ;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;


% ----------------------- II.6 Battery and POWER  -------------------------
marginPercent = 0.05 ;  % Top and bottom plotting margin in % [-]

figure() ;
subplot 311 
plot(time, uav_Battery) ; grid on ; hold on ; legend("Battery Voltage [V]")
xlabel("Time [s]") ; ylabel("Voltage [V]") ;
currentYLim = ylim ;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;

subplot 312
plot(time, sum(uav_MotorCurrents,2)) ; grid on ; hold on ; legend("Battery Current [A]")
xlabel("Time [s]") ; ylabel("Current [A]") ;
currentYLim = ylim ;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;


subplot 313
plot(time, uav_Battery.*sum(uav_MotorCurrents,2)) ; grid on ; hold on ; legend("Electric POWER [W]")
xlabel("Time [s]") ; ylabel("Power [W]") ;
currentYLim = ylim ;
dataRange = currentYLim(2) - currentYLim(1) ;
marginValue = dataRange * marginPercent ;
ylim([currentYLim(1) - marginValue, currentYLim(2) + marginValue]) ;


